
package restful;

import java.sql.Connection;
import java.sql.DriverManager;
public class VerifyConexion {
    
    private boolean bandera = true;
    private Connection con;
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String HOST = "localhost:3306";
    private String DB = "almacen";
    private String URL = "jdbc:mysql://" + HOST + "/" + DB;
    private String USERNAME = "root";
    private String PASSWORD = "";
    
    public VerifyConexion() {
        if (bandera == false) {
            DB = "c3s56grupo1";
            USERNAME = "c3s56grupo1";
            PASSWORD = "0ndcopHj";
            URL = "jdbc:mysql://" + HOST + "/" + DB;
        }
        
        try {
            Class.forName(DB_DRIVER);
            con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            System.out.println("Conexión Exitosa");
        } catch (Exception e) {
            System.out.println("Error de Conexión" + e.getMessage());
        }
    }
    
    public Connection getCon() {
        return con;
    }
    
    public void desconetar() {
        try {
            if(con != null) {
                con.close();
                System.out.println("La desconexion fue exitosa");
            }
        } catch (Exception e) {
            System.out.println("Ha ocurrido un error al desconectar" + e.getMessage());
        }
    }
    
    public String prueba() {
        if (bandera == false) {
            DB = "c3s56grupo1";
            USERNAME = "c3s56grupo1";
            PASSWORD = "0ndcopHj";
            URL = "jdbc:mysql://" + HOST + "/" + DB;
        }
        
        try {
            Class.forName(DB_DRIVER);
            con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            return ("Metadata = " + con.getMetaData()+
                    "\nDriver = "+con.getSchema());
        } catch (Exception e) {
            return "Error de Conexión" + e.getMessage();
        }
    }
    
}
